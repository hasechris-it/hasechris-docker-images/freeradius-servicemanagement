FROM httpd:2.4

ENV FREERADIUS_CONTAINER_NAME="freeradius"

COPY --from=docker:dind /usr/local/bin/docker /usr/local/bin
RUN rm /usr/local/apache2/cgi-bin/printenv* && \
    rm /usr/local/apache2/cgi-bin/test-cgi
COPY index.css /usr/local/apache2/cgi-bin/index.css
COPY index.cgi /usr/local/apache2/cgi-bin/index.cgi
COPY httpd.conf /usr/local/apache2/conf/httpd.conf
RUN  mkdir /dynamic && \
     chown www-data:www-data /dynamic && \
     chown www-data:www-data /usr/local/apache2/cgi-bin/index.cgi && \
     chown www-data:www-data /usr/local/apache2/cgi-bin/index.css && \
     chmod 755 /usr/local/apache2/cgi-bin/index.cgi && \
     chmod 644 /usr/local/apache2/cgi-bin/index.css && \
     groupadd -g 999 docker && \
     usermod -a -G 999 www-data && \
     rm /usr/local/apache2/bin/suexec
